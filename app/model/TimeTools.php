<?php

/***********************************************************************************************************************
 *
 * This file is part of the ${PROJECT_NAME} project

 * ==========================================
 *
 * Copyright (C) ${YEAR} by University of West Bohemia (http://www.zcu.cz/en/)
 *
 ***********************************************************************************************************************
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 ***********************************************************************************************************************
 *
 * ${NAME}, ${YEAR}/${MONTH}/${DAY} ${HOUR}:${MINUTE} ${USER}
 *
 **********************************************************************************************************************/

namespace App\Model;

use Nette;


/**
 * Time tools. Requires PHP >= 5.3.7.
 */
class TimeTools {

	/**
	 * @param  string
	 * @param  array with cost (4-31), salt (22 chars)
	 * @return string  60 chars long
	 */
	public static function sum($runs) {
		$suma = 0;	
		foreach ($runs as $line) {
			$suma += TimeTools::time_to_int($line->__get("time"));
		}
		$hours = $suma / 3600;
		$suma %= 3600;
		$minutes = ($suma / 60);
		$suma %= 60;
		return floor($hours).":".floor($minutes).":".$suma;
	}

	private static function time_to_int($duration) {
		$seconds = 0;

		$seconds += $duration->h*3600;
		$seconds += $duration->i*60;
		$seconds += $duration->s;

		return $seconds;
	}

}
