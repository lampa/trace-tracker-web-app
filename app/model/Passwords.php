<?php
/***********************************************************************************************************************
 *
 * This file is part of the ${PROJECT_NAME} project

 * ==========================================
 *
 * Copyright (C) ${YEAR} by University of West Bohemia (http://www.zcu.cz/en/)
 *
 ***********************************************************************************************************************
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 ***********************************************************************************************************************
 *
 * ${NAME}, ${YEAR}/${MONTH}/${DAY} ${HOUR}:${MINUTE} ${USER}
 *
 **********************************************************************************************************************/

/**
 * This file is part of the Nette Framework (http://nette.org)
 * Copyright (c) 2004 David Grudl (http://davidgrudl.com)
 */

namespace App\Model;

use Nette;


/**
 * Passwords tools. Requires PHP >= 5.3.7.
 *
 * @author     David Grudl
 */
class Passwords
{
	const PASSWORD_MAX_LENGTH = 4096;
	const BCRYPT_COST = 10;


	/**
	 * Computes salted password hash.
	 * @param  string
	 * @param  array with cost (4-31), salt (22 chars)
	 * @return string  60 chars long
	 */
	public static function hash($password, array $options = NULL)
	{
		$cost = isset($options['cost']) ? (int) $options['cost'] : self::BCRYPT_COST;
		$salt = isset($options['salt']) ? (string) $options['salt'] : self::salt();

		if (PHP_VERSION_ID < 50307) {
			throw new Nette\NotSupportedException(__METHOD__ . ' requires PHP >= 5.3.7.');
		} elseif (($len = strlen($salt)) < 22) {
			throw new Nette\InvalidArgumentException("Salt must be 22 characters long, $len given.");
		} elseif ($cost < 4 || $cost > 31) {
			throw new Nette\InvalidArgumentException("Cost must be in range 4-31, $cost given.");
		}

		$password = substr($password, 0, self::PASSWORD_MAX_LENGTH);
		$hash = crypt($password, '$2y$' . ($cost < 10 ? 0 : '') . $cost . '$' . $salt);
		if (strlen($hash) < 60) {
			throw new Nette\InvalidStateException('Hash returned by crypt is invalid.');
		}
		return $hash;
	}

	/**
	 * Generate random salt.
	 * @return String (22 chars)
	 */
	private static function salt() {
		return Nette\Utils\Strings::random(22, '0-9A-Za-z./');
	}

	/**
	 * Verifies that a password matches a hash.
	 * @return bool
	 */
	public static function verify($password, $hash)
	{
		return preg_match('#^\$2y\$(?P<cost>\d\d)\$(?P<salt>.{22})#', $hash, $m)
			&& $m['cost'] > 3 && $m['cost'] < 31
			&& self::hash($password, $m) === $hash;
	}


	/**
	 * Checks if the given hash matches the options.
	 * @param  string
	 * @param  array with cost (4-31)
	 * @return bool
	 */
	public static function needsRehash($hash, array $options = NULL)
	{
		$cost = isset($options['cost']) ? (int) $options['cost'] : self::BCRYPT_COST;
		return !preg_match('#^\$2y\$(?P<cost>\d\d)\$(?P<salt>.{22})#', $hash, $m)
			|| $m['cost'] < $cost;
	}

}
