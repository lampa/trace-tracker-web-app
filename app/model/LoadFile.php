
<?php


namespace App\Model;

/***********************************************************************************************************************
 *
 * This file is part of the ${PROJECT_NAME} project

 * ==========================================
 *
 * Copyright (C) ${YEAR} by University of West Bohemia (http://www.zcu.cz/en/)
 *
 ***********************************************************************************************************************
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 ***********************************************************************************************************************
 *
 * ${NAME}, ${YEAR}/${MONTH}/${DAY} ${HOUR}:${MINUTE} ${USER}
 *
 **********************************************************************************************************************/


/** @var Nette\Database\Context */
	
/**
 * Time tools. Requires PHP >= 5.3.7.
 */
class LoadFile  {




	public static function xml_parser($log) {
		
		$xml = simplexml_load_file($log);
		
		
		$data = explode(";", $xml->data);

		if (count($data)== null ) {
			return null;
		}

		$chartdata = array();
		$jednotky = "sec";
		if(count($data)>=120){
			$jednotky ="min";
			$iterace = floor(count($data)/60);
			$zbytek = count($data)-60*$iterace;
			$temp = array();
			$temp[0]=str_replace(',', '.', $data[0]);
			for($i = 1; $i < $iterace+1; $i++){
				$soucet =0;
				for($j = ($i-1)*60; $j < $i*60;$j++){
					
					$soucet +=str_replace(',', '.', $data[$j]);


				}

				$temp[$i]=$soucet/60;
			}
			if($zbytek>0){
				$soucet =0;
				for($i=60*$iterace; $i <count($data);$i++){
					$soucet += str_replace(',', '.', $data[$i]);
				}
				$temp[count($temp)] =$soucet/$zbytek;
			}


			$data=$temp;
		}
		$j = 0;
		for ($i=0; $i < count($data) ; $i++) { 
			$chartdata[] = array('hodnota'=>str_replace(',', '.', $data[$i]),'cas'=>$j, 'jednotky'=>$jednotky);
			if($jednotky=="sec"){
			$j += 2;
		}else{
			$j +=1;
		}
		}
		return json_encode($chartdata);
	}
	/*
	*Tohle kdyztak dopisu jestli se ti to nebude chtit delat
	*/
	public static function xml_parser_souradnice($log) {
		
		//$xml = simplexml_load_file("../records/".$jmeno."/".$run->log);
		$xml = simplexml_load_file($log);
		$j = 0;
		$data = explode(";", $xml->coordinate);
		if (count($data)== null ) {
			return null;
		}
		$mapdata = array();
		for ($i=0; $i < count($data)-1 ; $i++) { 
			$dato = explode(":", $data[$i]);
			//echo count($dato);
			$mapdata[] = array("lat"=>str_replace(',', '.', $dato[0]),"longt"=>$dato[1]);
			
		}
		//echo json_encode($mapdata);

		return json_encode($mapdata);
	}

	

}
