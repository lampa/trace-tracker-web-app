<?php
/***********************************************************************************************************************
 *
 * This file is part of the ${PROJECT_NAME} project

 * ==========================================
 *
 * Copyright (C) ${YEAR} by University of West Bohemia (http://www.zcu.cz/en/)
 *
 ***********************************************************************************************************************
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *s
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 ***********************************************************************************************************************
 *
 * ${NAME}, ${YEAR}/${MONTH}/${DAY} ${HOUR}:${MINUTE} ${USER}
 *
 **********************************************************************************************************************/

namespace App\Presenters;

use Nette,
	App\Model,
	App\Model\TimeTools;


/**
 * Homepage presenter.
 */
class ProfilePresenter extends BasePresenter {
	/** @var Nette\Database\Context */
	private $database;

	public function __construct(Nette\Database\Context $database) {
		$this->database = $database;
	}

	public function actionDefault() {
		if (!$this->user->isLoggedIn()) {
			$this->redirect('Sign:in');
		}
	}

	public function renderDefault()	{
		$tableUser = $this->database->table('users')->get($this->user->getId());
		if($tableUser->name != null) {
			$this->template->userName = $tableUser->name;
		}
		
		$this->template->userMail = $tableUser->mail;
		$this->template->runs = $tableUser->related('runs')->order('datetime DESC');
		$this->template->sumTime = TimeTools::sum($this->template->runs);
	}

}
