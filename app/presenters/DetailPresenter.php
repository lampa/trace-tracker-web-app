<?php 
/***********************************************************************************************************************
 *
 * This file is part of the ${PROJECT_NAME} project

 * ==========================================
 *
 * Copyright (C) ${YEAR} by University of West Bohemia (http://www.zcu.cz/en/)
 *
 ***********************************************************************************************************************
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 ***********************************************************************************************************************
 *
 * ${NAME}, ${YEAR}/${MONTH}/${DAY} ${HOUR}:${MINUTE} ${USER}
 *
 **********************************************************************************************************************/

namespace App\Presenters;

use Nette,
	App\Model, App\Model\LoadFile;


class DetailPresenter extends BasePresenter {
	
	/** @var Nette\Database\Context */
	private $database;

	public function __construct(Nette\Database\Context $database) {
		$this->database = $database;
	}

	public function actionShow($runId) {
		if ($this->database->table('runs')->get($runId)->user->id != $this->user->getId()) {
			$this->error('Nemáte oprávnění pro přístup na tento záznam');
		}
	}

	public function renderShow($runId) {
		$run = $this->database->table('runs')->get($runId);
		if (!$run) {
			$this->error('Stránka nebyla nalezena');
		}

		$this->template->run = $run;
		$this->template->data = LoadFile::xml_parser($run->log);
		$this->template->dataMapa = LoadFile::xml_parser_souradnice($run->log);
	}

}
