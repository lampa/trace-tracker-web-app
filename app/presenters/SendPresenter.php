<?php
/***********************************************************************************************************************
 *
 * This file is part of the ${PROJECT_NAME} project

 * ==========================================
 *
 * Copyright (C) ${YEAR} by University of West Bohemia (http://www.zcu.cz/en/)
 *
 ***********************************************************************************************************************
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 ***********************************************************************************************************************
 *
 * ${NAME}, ${YEAR}/${MONTH}/${DAY} ${HOUR}:${MINUTE} ${USER}
 *
 **********************************************************************************************************************/

namespace App\Presenters;

use Nette,
	DateTime,
	Nette\Http\Request,
	App\Model;


/**
 * Send presenter.
 */
class SendPresenter extends BasePresenter {
	/** @var Nette\Database\Context */
	private $database;

	public function __construct(Nette\Database\Context $database) {
		$this->database = $database;
	}

	public function actionDefault($username, $password) {
		try {
			if (!$this->user->isLoggedIn()) {			
				$this->getUser()->login($username, $password);
			}
			$post = $this->getHttpRequest()->getPost();
			if(!isset($post['xml'])) {
				$this->error('Nebyla predana pozadovana data');
			}
			$array = $this->inputParser($post['xml']);
			
			$duplicityControl = $this->database->table('users')->get($this->user->getId())->related('runs')->where('hash', $array['hash']);
			if(count($duplicityControl) > 0) {
				$this->error('Soubor je již nahrán');
			}

			$url = $this->saveFile(strtolower($username), $post['xml'], $array['hash']);
			$this->makeInsert(DateTime::createFromFormat('Y-m-d H:i:s', $array['start']), $url, $array['distance'], $array['time'], $array['comment'], $array['hash']);
		} catch (Nette\Security\AuthenticationException $e) {
			$this->error($e->getMessage());
		}
	}
	
	public function renderDefault($username, $password)	{
	}

	private function saveFile($username, $post, $filename) {
		if(!file_exists(RECORDS_DIR."/".$username)) {
			mkdir(RECORDS_DIR."/".$username, 0777, true);
		}
		$file = fopen(RECORDS_DIR."/".$username."/".$filename.".xml", 'w');
		fwrite($file, $post);
		fclose($file);
		return RECORDS_DIR."/".$username."/".$filename.".xml";
	}

	private function makeInsert($datetime, $url, $length, $time, $description, $hash) {
		$this->database->table('runs')->insert(array(
			'datetime' => $datetime,
			'log' => $url,
			'length' => $length,
			'time' => $time,
			'description' => $description,
			'hash' => $hash,
			'USERS_id' => $this->user->getId(),
		));
	}

	private function inputParser($input) {
		return array(
			'hash' => $this->getContent($input, 'hash'),
			'start' => $this->getContent($input, 'start'),
			'steps' => $this->getContent($input, 'steps'),
			'distance' => $this->getContent($input, 'distance'),
			'time' => $this->getContent($input, 'time'),
			'comment' => $this->getContent($input, 'comment'),
			//'data' => getContent($input, 'data'),
			//'coordinates' => getContent($input, 'coordinates'),
		);
	}

	private function getContent($string, $tag) {
		$pattern = "/<".$tag.">(.*?)<\/".$tag.">/si";
		preg_match_all($pattern, $string, $array);
		$leftCut = explode(">", $array[0][0], 2);
		$output = explode("</", $leftCut[1], 2);	
		return $output[0];
	}
}
