<?php 
/***********************************************************************************************************************
 *
 * This file is part of the ${PROJECT_NAME} project

 * ==========================================
 *
 * Copyright (C) ${YEAR} by University of West Bohemia (http://www.zcu.cz/en/)
 *
 ***********************************************************************************************************************
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 ***********************************************************************************************************************
 *
 * ${NAME}, ${YEAR}/${MONTH}/${DAY} ${HOUR}:${MINUTE} ${USER}
 *
 **********************************************************************************************************************/

namespace App\Presenters;

use Nette,
	App\Model,
	App\Model\Passwords,
	Nette\Application\UI\Form;


class RegistrationPresenter extends BasePresenter {
	
	/** @var Nette\Database\Context */
	private $database;

	public function __construct(Nette\Database\Context $database) {
		$this->database = $database;
	}


	public function createComponentRegisterForm() {
		$form = new Form;

		$form->addText('username', 'Uživatelské jméno:')
			->setRequired('Prosím, zadejte uživatelské jméno.')
			->addRule(Form::PATTERN, 'Pro uživatelské jméno použijte pouze alfa-numerické znaky, _ a -!' ,'[a-zA-Z][a-zA-Z0-9_\-]*');

		$form->addPassword('password', 'Heslo:')
			->setRequired('Prosím, zadejte heslo.');

		$form->addText('email', 'Email:')
			->setRequired('Prosím, zadejte Váš email.')
			->addRule(Form::EMAIL, 'Emailová adresa nebyla zadána ve validním stavu');

		$form->addText('name', 'Jméno:');


		$form->addSubmit('send', 'Registrovat');

		$form->onSuccess[] = $this->registerFormSucceeded;


		return $form;
	}

	public function registerFormSucceeded($form) {
		$values = $form->getValues();

		$this->database->table('users')->insert(array(
			'username' => $values->username,
			'password' => Passwords::hash(md5($values->password)),
			'mail' => $values->email,
			'name' => $values->name,
		));

		$this->flashMessage('Úspěšně registrován', 'success');
		$this->redirect('this');
	}
}
