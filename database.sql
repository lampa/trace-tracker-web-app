SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE SCHEMA IF NOT EXISTS `TraceTracker` DEFAULT CHARACTER SET utf16 ;
USE `TraceTracker` ;

-- -----------------------------------------------------
-- Table `TraceTracker`.`users`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `TraceTracker`.`users` ;

CREATE TABLE IF NOT EXISTS `TraceTracker`.`users` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(35) CHARACTER SET 'utf16' NOT NULL,
  `mail` VARCHAR(45) CHARACTER SET 'utf16' NOT NULL,
  `password` VARCHAR(60) CHARACTER SET 'utf16' NOT NULL,
  `name` VARCHAR(45) CHARACTER SET 'utf16' NULL DEFAULT NULL,
  `role` VARCHAR(10) CHARACTER SET 'utf16' NOT NULL DEFAULT 'user',
  PRIMARY KEY (`id`),
  UNIQUE INDEX `nickname_UNIQUE` (`username` ASC),
  UNIQUE INDEX `mail_UNIQUE` (`mail` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf16;


-- -----------------------------------------------------
-- Table `TraceTracker`.`runs`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `TraceTracker`.`runs` ;

CREATE TABLE IF NOT EXISTS `TraceTracker`.`runs` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `datetime` DATETIME NOT NULL,
  `log` VARCHAR(45) CHARACTER SET 'utf16' NOT NULL,
  `length` MEDIUMINT(9) NOT NULL,
  `time` TIME NOT NULL,
  `description` VARCHAR(200) CHARACTER SET 'utf16' NULL DEFAULT NULL,
  `USERS_id` BIGINT(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `log_UNIQUE` (`log` ASC),
  INDEX `fk_RUN_USERS_idx` (`USERS_id` ASC),
  CONSTRAINT `fk_RUN_USERS`
    FOREIGN KEY (`USERS_id`)
    REFERENCES `TraceTracker`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf16;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
